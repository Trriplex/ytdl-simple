(defsystem #:ytdl-simple
  :version "0.0.2"
  :license "BSD"
  :description "A simple youtube video downloader using LTK and youtube-dl"
  :author "Trriplex <tld29@outlook.fr>"
  :depends-on (:ltk :dexador :do-urlencode :jonathan)
  :components ((:file "ytdl-simple")))
