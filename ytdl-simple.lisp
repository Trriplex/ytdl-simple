(defpackage ytdl-simple
  (:use :cl :ltk)
  (:export main))
(in-package :ytdl-simple)

(defmacro make-label (label-name)
  `(make-instance 'label :text ,label-name :master main-frame))

(defmacro make-entry (entry-name)
  `(make-instance 'entry :text ,entry-name :master main-frame))

(defmacro make-button (button-name command &optional command-arg)
  `(make-instance 'button :text ,button-name
                  :master main-frame
                  :command (lambda ()
                             ,(if command-arg
                                  `(funcall ,command ,command-arg)
                                  `(funcall ,command)))))

(defmacro add-song (song)
  `(lambda ()
     (add-title ,song)
     (setf (text video-entry) "")
     (setf (text video-list) (format nil "~{~a~%~}" *video-titles*))))

(defmacro remove-song ()
  `(lambda ()
     (pop *video-list*)
     (pop *video-titles*)
     (setf (text video-list) (format nil "~{~a~%~}" *video-titles*))))

(defparameter *video-list* (list))
(defparameter *ytdl-program* "yt-dlp")
(defparameter *dir* "~/Musique")
(defparameter *video-titles* (list))

(defun setup-theme ()
  (send-wish "source azure.tcl")
  (send-wish "set_theme dark"))

(defun add-title (url)
  (handler-case
    (multiple-value-bind (body)
      (dex:get (concatenate 'string
                            "https://youtube.com/oembed?format=json&url="
                            (do-urlencode:urlencode url)))
      (push url *video-list*)
      (push (getf (jonathan:parse body) :|title|) *video-titles*))
    (t (c)
       (declare (ignore c))
       (do-msg "Erreur: l'addresse spécifiée est invalide"))))

(defun set-program (program)
  (setf *ytdl-program* program)
  (do-msg "Le programme de téléchargement a été remplacé" :title "OK"))

(defun set-dir (dir)
  (if (probe-file dir)
      (progn
        (do-msg "Le dossier de téléchargement a été remplacé" :title "OK") 
        (setf *dir* dir))
      (do-msg "Le dossier de téléchargement n'est pas valide" :title "Erreur")))

(defun dl-songs ()
  (if (equalp *video-list* (list))
      (do-msg "Il n'y a aucune musique à télécharger !")
      (handler-case
        (progn
          (uiop:chdir *dir*)
          (do-msg "Téléchargement en cours...")
          (uiop:run-program (append (list *ytdl-program*)
                                    '("-x" "--audio-format" "mp3")
                                   *video-list*))
          (do-msg "Téléchargement achevé !"))
        (t (c)
           (do-msg (format nil "Erreur: ~a" c) :title "Erreur fatale")))))

(defun main ()
  (with-ltk ()
    (let* ((main-frame (make-instance 'frame))
           ; Labels
           (ytdl-label (make-label "Programme d'extraction: "))
           (dir-label (make-label "Dossier de téléchargement: "))
           (video-label (make-label "Adresse de la Vidéo: "))
           (video-list (make-label ""))
           ; Entries
           (ytdl-entry (make-entry "yt-dlp"))
           (dir-entry (make-entry "~/Musique"))
           (video-entry (make-entry ""))
           ; Buttons
           (ytdl-button (make-button "Changer" #'set-program (text ytdl-entry)))
           (dir-button (make-button "Changer" #'set-dir (text dir-entry)))
           (video-add (make-button "Ajouter" (add-song (text video-entry))))
           (video-pop (make-button "Enlever" (remove-song)))
           (video-dl (make-button "Télécharger" #'dl-songs)))
      (setup-theme)
      (pack main-frame)
      (grid ytdl-label 0 0)
      (grid ytdl-entry 0 1)
      (grid ytdl-button 0 2 :padx 20)
      (grid dir-label 1 0)
      (grid dir-entry 1 1)
      (grid dir-button 1 2)
      (grid (make-label (format nil "~%")) 2 1)
      (grid video-label 3 0)
      (grid video-entry 3 1)
      (grid video-add 4 0 :sticky "n")
      (grid video-pop 4 2 :sticky "n")
      (grid video-list 4 1)
      (grid video-dl 5 1))))
